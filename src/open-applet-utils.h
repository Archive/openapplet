/* 
 * Copyright (C) 2003 David Bordoley
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifndef OPEN_APPLET_UTILS_H
#define OPEN_APPLET_UTILS_H

#include <gtk/gtk.h>

/* input-parsing */

gchar		*open_applet_utils_compute_url			(const gchar *input);
gboolean	 open_applet_utils_is_command			(const gchar *input);

/* gui-helpers */

GtkWidget	*open_applet_utils_build_hig_alert		(const gchar *primary_text, 
								 const gchar *secondary_text,
								 const gchar *stock_id);

void		 open_applet_utils_help				(GtkWindow  *parent,
								 const char *file_name,
								 const char *link_id);

void		 open_applet_utils_url_show			(const gchar *url);

void		 open_applet_utils_run_command			(const gchar *command, 
								 GdkScreen  *screen, 
								 gboolean use_terminal);

			
#endif /*OPEN_APPLET_UTILS_H*/
