/* 
 * Copyright (C) 2003 David Bordoley
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "open-applet-utils.h"
#include "egg-screen-exec.h"

#include <libgnomevfs/gnome-vfs-mime-utils.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <libgnome/gnome-help.h>
#include <libgnome/gnome-exec.h>
#include <bonobo/bonobo-i18n.h>

#define FILE_TYPES_CAPPLET_NAME 	"gnome-file-types-properties"

typedef struct
{
	gchar *mime_type;
	gchar *file_path;
} MimeCBData;

enum
{
	OPEN_APPLET_RESPONSE_LAUNCH_MIME_CAPPLET
};

static gboolean
open_applet_utils_launch_mime_capplet (const gchar *mime_type, 
				       const gchar *file_path)
{
	gchar *command;
	gboolean retval;

	if (file_path)
	{
		command = g_strconcat (FILE_TYPES_CAPPLET_NAME, " ", 
				       mime_type == NULL ? "" : mime_type, " ", 
				       file_path, NULL);
		retval = g_spawn_command_line_async (command, NULL);
		g_free (command);
		return retval;
	}
	return FALSE;
}

static void
error_message_cb (GtkDialog *dialog, gint response_id, MimeCBData *data)
{
	switch (response_id)
	{
		case GTK_RESPONSE_OK:
			break;
		case  OPEN_APPLET_RESPONSE_LAUNCH_MIME_CAPPLET:
			open_applet_utils_launch_mime_capplet (data->mime_type, data->file_path);
		default:
			break;
	}
	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
free_mime_cb_data (MimeCBData *data)
{
	if (data)
	{
		g_free (data->mime_type);
		g_free (data->file_path);
		g_free (data);
	}
}

static void
display_error_message (const gchar *user_input, GnomeVFSResult result)
{
	GtkWidget *dialog;
	GtkWidget *button;
	gchar *primary_text;
	gchar *secondary_text;
	MimeCBData *data = NULL;

	primary_text = g_strdup_printf (_("Could not open \"%s.\""), user_input);

	/* FIXME: I'm not all that thrilled with the error messages 
	 * gnome-vfs provides here. Perhaps we should create our own strings
	 * based on the vfs results. Another thing is that for the unable to
	 * launch associated application error, we ought to provide a button
	 * thats opens the correct mime-type capplet so that the user can
	 * fix the problem.
	 */ 
	secondary_text = g_strdup_printf (
		_("The error \"%s\" was received when trying to open \"%s.\""),
		gnome_vfs_result_to_string (result),
		user_input);

	dialog = open_applet_utils_build_hig_alert (primary_text, 
		 				    secondary_text,
		 				    GTK_STOCK_DIALOG_ERROR);

	if (result == GNOME_VFS_ERROR_NO_DEFAULT)
	{
		gchar *mime_type;

		gtk_dialog_add_button (GTK_DIALOG (dialog),
						_("Associate Application"),
						OPEN_APPLET_RESPONSE_LAUNCH_MIME_CAPPLET);

		/* FIXME: Sync I/o is bad */
		mime_type = gnome_vfs_get_mime_type (user_input);

		data = g_new0 (MimeCBData, 1);
		data->mime_type = mime_type;
		data->file_path = g_strdup (user_input);		
	}

	button = gtk_button_new_from_stock (GTK_STOCK_OK);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_OK);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	gtk_widget_show (button);

	gtk_widget_show (dialog);
	g_signal_connect_closure
		(G_OBJECT (dialog), "response",
		 g_cclosure_new (G_CALLBACK (error_message_cb), 
				  data,
				  (GClosureNotify)free_mime_cb_data),
		FALSE);

	g_free (primary_text);
	g_free (secondary_text);
}


/************************************* Public API ****************************************/

void
open_applet_utils_url_show (const gchar *url)
{
	GnomeVFSResult result;
	g_return_if_fail (url != NULL);

	result = gnome_vfs_url_show (url);

	if (result != GNOME_VFS_OK)
	{
		display_error_message (url, result);
	}
}

/* Mostly copied from panel-run-dialog.c */
void
open_applet_utils_run_command (const gchar *command, GdkScreen *screen, gboolean use_terminal)
{		
	gchar     **argv;
	gchar     **envp;
	gint        argc;	
	gboolean    result;
	GError     *error = NULL;

	if (!open_applet_utils_is_command (command))
		return;

	argv = g_strsplit (command, " ", 0);
	
	envp = egg_screen_exec_environment (screen);
		
	if (use_terminal)
	{
		gnome_prepend_terminal_to_vector (&argc, &argv);
	}
		   
	result = g_spawn_async (NULL, /* working directory */
				argv,
				envp,
				G_SPAWN_SEARCH_PATH,
				NULL, /* child setup func */
				NULL, /* user data */
				NULL, /* child pid */
				&error);

	/* FIXME: Probably should check result if to see if the command
	 *        succeeded and if not pop up an error dialog
	 */
}

void
open_applet_utils_help (GtkWindow  *parent,
			const char *file_name,
			const char *link_id)
{
	GError *err = NULL;

	gnome_help_display (file_name, link_id, &err);

	if (err != NULL)
	{
		GtkWidget *dialog;
		GtkWidget *button;

		dialog = open_applet_utils_build_hig_alert (_("Could not display help."),
							    err->message,
							    GTK_STOCK_DIALOG_ERROR);
		button = gtk_button_new_from_stock (GTK_STOCK_OK);
		gtk_dialog_add_action_widget (GTK_DIALOG (dialog), button, GTK_RESPONSE_OK);
		gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);

		g_signal_connect (G_OBJECT (dialog), "response",
				  G_CALLBACK (gtk_widget_destroy),
				  NULL);

		gtk_widget_show (button);
		gtk_widget_show (dialog);
		g_error_free (err);
	}
}

/* NOTE: Building a custom dialog here sucks. Maybe
 * GTK will one day provide an easy api for building dialogs.
 * gtk_message_dialog doesn't provide an api for HIG friendlyness.
 */
GtkWidget *
open_applet_utils_build_hig_alert (const gchar *primary_text, 
				   const gchar *secondary_text,
				   const gchar *stock_id)
{
	GtkWidget *dialog;
	GtkWidget *label;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *image;
	gchar *str;

	dialog = gtk_dialog_new_with_buttons (primary_text,
					      NULL,
					      GTK_DIALOG_DESTROY_WITH_PARENT |
					      GTK_DIALOG_NO_SEPARATOR,
					      NULL);
	gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 5);
	gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 14);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox,
			    TRUE, TRUE, 0);

	image = gtk_image_new_from_stock (stock_id, GTK_ICON_SIZE_DIALOG);
	gtk_misc_set_alignment (GTK_MISC (image), 0.5, 0.0);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	str = g_strconcat ("<b><big>", primary_text, "</big></b>", NULL);
	gtk_label_set_markup (GTK_LABEL (label), str);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);

	label = gtk_label_new (secondary_text);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
	
	gtk_widget_show_all (hbox);

	return dialog;
}
