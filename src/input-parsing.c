/* 
 * Copyright (C) 2003 David Bordoley
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include "open-applet-utils.h"
#include <string.h>
#include <libgnomevfs/gnome-vfs.h>

static gboolean
is_url (const gchar *input)
{
	gchar *scheme;

	scheme = gnome_vfs_get_uri_scheme (input);

	/* We assume that if the user provides input 
	 * with a scheme than it's a url.
	 */
	if (scheme)
	{
		g_free (scheme);
		return TRUE;
	}

	return FALSE;
}

/* Really crude hack.
 * If the input has any ' ' characters fail.
 * If the input has no '.' characters fail.
 */
static gboolean
is_web_address (const gchar *input)
{
	int length = strlen (input);
	int i;
	int period_count = 0;

	for (i = 0; i < length; i++)
	{
		if (input[i] == ' ')
		{
			return FALSE;
		}
		else if (input[i] == '.')
		{
			period_count++;
		}
	}

	if (period_count > 0)
	{
		return TRUE;
	}

	return FALSE;
}

static gboolean
is_email_address (const gchar *input_copy)
{
	int length = strlen (input_copy);
	int i;
	int amphersand_count = 0;

	if (is_web_address (input_copy))
	{
		for (i = 0; i < length; i++)
		{
			if (input_copy[i] == '@')
			{
				amphersand_count++;
			}

			if (amphersand_count > 1)
			{
				return FALSE;
			}
		}
	}

	if (amphersand_count < 1)
	{
		return FALSE;
	}
	return TRUE;
}

/* Public Functions */

/* Guaranteed to return a valid url, otherwise will return NULL */
gchar*
open_applet_utils_compute_url (const gchar *input)
{
	gchar *input_copy = g_strdup (input);
	gchar *url = NULL;		
	gchar *retval = NULL;	/* Return NULL if we can't compute a URL */

	/* Remove any leading or trailing white space */
	g_strstrip (input_copy);

	/* FIXME: This doesn't take into account the case where a user just supplies
	 * a scheme.
	 */
	/* Check if the input is a url */
	if (is_url (input_copy))
	{
		url = g_strdup (input_copy);
	}

	/* If the first character is '/', 
	 * we assume that its a local path 
	 */
	else if (input_copy[0] == '/')
	{
		url = gnome_vfs_get_uri_from_local_path (input_copy);
	}

	/* If the first character is '~', 
	 * we assume that its a local path 
	 * starting in the homedir. 
	 */
	else if (input_copy[0] == '~')
	{ 
		gchar *temp;
	
		temp = gnome_vfs_expand_initial_tilde (input_copy);
		url = gnome_vfs_get_uri_from_local_path (temp);
	
		g_free (temp);
	}

	/* Check for common prefixes */
	else if (g_str_has_prefix (input_copy, "irc."))
	{
		url = g_strconcat("irc://", input_copy, NULL); 
	}
	else if (g_str_has_prefix (input_copy, "ftp."))
	{
		url = g_strconcat("ftp://", input_copy, NULL); 
	}
	else if (g_str_has_prefix (input_copy, "www."))
	{
		url = g_strconcat("http://", input_copy, NULL); 
	}

	/* Check if the input is an email address. */
	else if (is_email_address (input_copy))
	{
		url = g_strconcat("mailto://", input_copy, NULL);
	}	

	/* Check if the input is something resembling 
	 * a web address. If so append http:// to it and
	 * hope it works.
	 */ 
	else if (is_web_address(input_copy))
	{
		url = g_strconcat("http://", input_copy, NULL);
	}
	g_free (input_copy);

	/* Escape the url */
	if (url)
	{
		retval = gnome_vfs_make_uri_from_input (url);
		g_free (url);
	}

	return retval;
}

static gboolean
command_in_path (const gchar *input, const char *dirname)
{
	gboolean retval = FALSE;	
	gchar *url;
	gchar *local_path;
	GnomeVFSFileInfo *file_info;
	GnomeVFSResult result;

	local_path = g_build_filename (dirname, input, NULL);
	url = gnome_vfs_get_uri_from_local_path (local_path);

	file_info = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info
			(url, file_info, 
			 GNOME_VFS_FILE_INFO_FORCE_FAST_MIME_TYPE | 				
			 GNOME_VFS_FILE_INFO_FOLLOW_LINKS         |
			 GNOME_VFS_FILE_INFO_GET_ACCESS_RIGHTS);

	if ((result == GNOME_VFS_OK) &&
	    (file_info->permissions & GNOME_VFS_PERM_ACCESS_EXECUTABLE))
	{
		retval = TRUE;
	}
    
	g_free (local_path); 
	g_free (url);
	gnome_vfs_file_info_unref (file_info);

	return retval;
}

/* Copied from panel-run-dialog.c */
gboolean
open_applet_utils_is_command (const gchar *input)
{	
	const gchar  *path;
	gchar       **pathv;
	gint          i;
	gboolean      retval = FALSE;
	gchar        *input_copy = g_strdup (input);
	gchar       **commandv;	
	
	/* FIXME: Detect if a string is all whitespace and return FALSE */
	/* FIXME: Detect if a string is '/' and return FALSE */
	/* FIXME: THis should return true if you provide an absolute
	 *        path to an executable file.
	 */


	g_strstrip (input_copy);
	commandv = g_strsplit (input_copy, " ", 2);

	path = g_getenv ("PATH");
	if (!path || !path [0])
		return FALSE;

	pathv = g_strsplit (path, ":", 0);

	for (i = 0; pathv [i]; i++)
	{
		retval = command_in_path (commandv[0], pathv[i]);
		
		if (retval)
			break;
	}
	
	g_strfreev (pathv);
	g_strfreev (commandv);
	g_free (input_copy);

	return retval;
}
