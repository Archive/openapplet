/* 
 * Copyright (C) 2003 David Bordoley
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "open-applet-utils.h"

#include <string.h>
#include <gtk/gtk.h>
#include <libgnomeui/gnome-about.h>
#include <bonobo/bonobo-i18n.h>
#include <panel-applet.h>

/* Perhaps we should have a preference for web search engine */
#define GOOGLE_SEARCH_URL		_("http://www.google.com/search?q=%s&ie=UTF-8&oe=UTF-8")

enum {SEARCH_WEB, RUN_COMMAND, RUN_COMMAND_TERMINAL};

typedef struct 
{
	PanelApplet base;
	
	GtkWidget *entry;
	GtkWidget *event_box;
	GdkScreen *screen;
} OpenApplet;

static void 	 open_applet_about_cb 		(BonoboUIComponent *uic,
						 OpenApplet *applet);
static void 	 open_applet_help_cb 		(BonoboUIComponent *uic,
						 OpenApplet *applet);

static const BonoboUIVerb open_applet_menu_verbs [] = {
	BONOBO_UI_UNSAFE_VERB ("OpenAppletHelp", open_applet_help_cb),
	BONOBO_UI_UNSAFE_VERB ("OpenAppletAbout", open_applet_about_cb),
	BONOBO_UI_VERB_END
};

static GType
open_applet_get_type (void)
{
	static GType type = 0;

	if (!type) 
	{
		static const GTypeInfo info = 
			{sizeof (PanelAppletClass),
			 NULL, NULL, NULL, NULL, NULL,
			 sizeof (OpenApplet),
			 0, NULL, NULL};

		type = g_type_register_static (PANEL_TYPE_APPLET, "Open_Applet", &info, 0);
	}

	return type;
}

static void
paste_received (GtkClipboard *clipboard,
		const gchar  *text,
		gpointer      data)
{
	GtkEntry *entry = GTK_ENTRY (data);

	if (text)
	{ 
		gint pos;
               
		pos = gtk_editable_get_position (GTK_EDITABLE (entry));
  		gtk_editable_insert_text (GTK_EDITABLE (entry), text, -1, &pos);
		gtk_editable_set_position (GTK_EDITABLE (entry), pos);
	}

	g_object_unref (entry);
}

/* copied from gtkentry */
static void
entry_paste (GtkEntry *entry,
	     GdkAtom   selection)
{
	g_object_ref (entry);
	gtk_clipboard_request_text (gtk_widget_get_clipboard (GTK_WIDGET (entry), selection),
				    paste_received, entry);
}

static void
add_completion_action (OpenApplet *applet, gint index)
{
	GtkEntryCompletion *completion;
	const gchar* entry_text;
	gchar *action_text = NULL;

	completion = gtk_entry_get_completion (GTK_ENTRY(applet->entry));
	
	switch (index)
	{
		case SEARCH_WEB:
			/* FIXME: We should probably limit the length of the text we are
		         *        appending.
			 */
			entry_text = gtk_entry_get_text (GTK_ENTRY (applet->entry));
			action_text = g_strdup_printf (_("Search the web for \"%s\""), entry_text);
			break;
		case RUN_COMMAND:
			action_text = g_strdup (_("Run command"));
			break;
		case RUN_COMMAND_TERMINAL:
			action_text = g_strdup (_("Run command in terminal"));
			break;
	}

	if (action_text)
	{
		gtk_entry_completion_insert_action_text (completion, index, action_text);		
		g_free (action_text);
	}
}

static void
remove_completion_action (OpenApplet *applet, gint index)
{
	GtkEntryCompletion *completion = gtk_entry_get_completion (GTK_ENTRY(applet->entry));

	gtk_entry_completion_delete_action (completion, index);
}

/************************************ Callbacks ************************************/
static gboolean
button_pressed_event_box_cb (GtkWidget		*widget,
			     GdkEventButton 	*event,
			     OpenApplet		*applet)
{
	if (event->button == 2)
	{
      		entry_paste (GTK_ENTRY (applet->entry), GDK_SELECTION_PRIMARY);
		return TRUE;	
	}
	else
	{
		gtk_widget_grab_focus (applet->entry);
		return FALSE;
	}	
} 
  
static void
activate_entry_cb (GtkWidget *entry, OpenApplet *applet)
{
	gchar *entry_text;
	gchar *url;

	entry_text = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
	gtk_entry_set_text (GTK_ENTRY (entry), "");

	url = open_applet_utils_compute_url (entry_text);
	g_free (entry_text);

	if (url)
	{
		open_applet_utils_url_show (url);
		g_free (url);
	}
	else
	{
		/* FIXME: Should display an error message 
		 * This is more important now that we aren't auto
		 * searching on not url.
		 */
	}	
}

static void
action_activated_cb (GtkEntryCompletion *completion,
                     gint                index,
		     OpenApplet         *applet)
{
	GtkWidget *entry;
	char *content;
	char *url;

	entry = gtk_entry_completion_get_entry (completion);
	content = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);
	
	if (content)
	{
		gtk_entry_set_text (GTK_ENTRY (entry), "");

		switch (index)
		{
			case SEARCH_WEB:
				url = g_strdup_printf (GOOGLE_SEARCH_URL, content);
				open_applet_utils_url_show (url);
				g_free (url);
				break;
			case RUN_COMMAND:
				open_applet_utils_run_command (content, applet->screen, FALSE); 
				break;	
			case RUN_COMMAND_TERMINAL:
				open_applet_utils_run_command (content, applet->screen, TRUE); 
				break;			
		}
		g_free (content);
	}
}

static void
changed_entry_cb (GtkEditable *editable, OpenApplet *applet)
{
	gchar *contents = gtk_editable_get_chars (editable, 0, -1);

	remove_completion_action (applet, RUN_COMMAND_TERMINAL);
	remove_completion_action (applet, RUN_COMMAND);
	remove_completion_action (applet, SEARCH_WEB);
	add_completion_action (applet, SEARCH_WEB);
	
	if (open_applet_utils_is_command (contents))
	{
		add_completion_action (applet, RUN_COMMAND);
		add_completion_action (applet, RUN_COMMAND_TERMINAL);
	}
	g_free (contents);
}

static gboolean
open_applet_destroy_cb (OpenApplet		*applet,
		        gpointer		 data)
{
	return FALSE;
}

static void
open_applet_help_cb (BonoboUIComponent 	*uic, 
		     OpenApplet 	*applet)
{ 
	open_applet_utils_help (NULL, "open-applet-help", NULL);
}

static void
open_applet_about_cb (BonoboUIComponent	*uic, 
		      OpenApplet 	*applet)
{
	static GtkWidget *about = NULL;
	GdkPixbuf *pixbuf;
	char *file;
	const gchar *authors[] = {
		"David Bordoley <bordoley@msu.edu>",
		NULL};
	const gchar *documenters[] = { NULL };
	const gchar *translator_credits = _("translator_credits");

	if (about != NULL)
	{
		gtk_widget_show (about);
		gtk_window_present (GTK_WINDOW (about));
		return;
	}

	file = gnome_program_locate_file (NULL, GNOME_FILE_DOMAIN_PIXMAP,
			"open-applet/open-applet-about.png", FALSE, NULL);
	pixbuf = gdk_pixbuf_new_from_file (file, NULL);
	g_free (file);

	about = gnome_about_new (
			_("Open Applet"),
			VERSION,
			_("(C) 2003 David Bordoley "),
			_("A text box to open stuff with"),
			authors,
			documenters,
			strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
			pixbuf);

	g_object_unref (pixbuf);
	
	gtk_widget_show (about);

	g_object_add_weak_pointer (G_OBJECT (about),
			(gpointer)&about);

	return;
}

/************************************ Build the GUI ************************************/

/* Set the width of the text entry to 1/8 of width of the screen */
static void
open_applet_set_entry_size (GdkScreen *screen,
			    OpenApplet *applet)
{	
	gint screen_width;
	gint entry_width;

	screen_width  = gdk_screen_get_width  (applet->screen);
	entry_width = (screen_width / 6);
	gtk_widget_set_size_request (applet->entry, entry_width, -1);
}

static void
open_applet_connect_signals (OpenApplet *applet)
{
	GtkEntryCompletion *completion;

	completion = gtk_entry_get_completion (GTK_ENTRY (applet->entry));

	g_signal_connect (G_OBJECT (applet->event_box), "button-press-event",
			  G_CALLBACK (button_pressed_event_box_cb), applet);
	g_signal_connect (G_OBJECT (applet->entry), "activate",
			  G_CALLBACK (activate_entry_cb), applet);
	g_signal_connect (G_OBJECT (applet->entry), "changed",
			  G_CALLBACK (changed_entry_cb), applet);
	g_signal_connect (G_OBJECT (applet->screen), "size-changed",
			  G_CALLBACK (open_applet_set_entry_size), applet);	
	g_signal_connect (G_OBJECT (completion), "action_activated",
			  G_CALLBACK (action_activated_cb), applet);
}

static void
open_applet_build (OpenApplet *applet)
{	
	GtkWidget *separator;
	GtkWidget *label;
	GtkWidget *entry;
	GtkWidget *event_box;
	GtkWidget *hbox;
	GtkTooltips *tooltips;	
	const gchar *tip = _("Enter a web address to open, or phrase to search for in your files and on the web.");
	
	separator = gtk_vseparator_new ();
	label = gtk_label_new (_("Open:"));
	entry = gtk_entry_new ();
	event_box = gtk_event_box_new ();
	tooltips = gtk_tooltips_new ();
	
	hbox = gtk_hbox_new (FALSE, 3);
	gtk_box_pack_start (GTK_BOX (hbox), separator, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), entry, FALSE, FALSE, 1);
	
	gtk_container_add (GTK_CONTAINER (event_box), hbox);
	gtk_container_add (GTK_CONTAINER (applet), event_box);

	gtk_widget_show_all (GTK_WIDGET (applet));
	
	applet->entry = entry;
	applet->event_box = event_box;
	
	gtk_tooltips_set_tip (tooltips, event_box, tip, NULL);       
}      

static void
open_applet_set_completion (OpenApplet *applet)
{
	GtkEntryCompletion *completion;

	completion = gtk_entry_completion_new ();
	gtk_entry_set_completion (GTK_ENTRY (applet->entry), completion);
	g_object_unref (completion);
}

static void
open_applet_set_screen (OpenApplet *applet)
{
	GtkWidget *parent_window;

	if (applet->screen == NULL)
	{
		parent_window = gtk_widget_get_toplevel (applet->entry);
		applet->screen = gtk_window_get_screen (GTK_WINDOW (parent_window));
	}
}

static gboolean
open_applet_new (OpenApplet *applet)
{
	open_applet_build (applet);
	open_applet_set_completion (applet);
	open_applet_set_screen (applet);
	open_applet_set_entry_size (applet->screen, applet);	
	open_applet_connect_signals (applet);
	return TRUE;
}

static gboolean
open_applet_factory (OpenApplet		*applet,
		     const gchar	*iid,
		     gpointer		 data)
{
	gboolean retval = FALSE;
	
	panel_applet_set_flags (PANEL_APPLET (applet), PANEL_APPLET_EXPAND_MINOR);
	
	if (!strcmp (iid, "OAFIID:GNOME_Open_Applet"))
		retval = open_applet_new (applet);
	
	g_signal_connect (G_OBJECT (applet), "destroy",
			  G_CALLBACK (open_applet_destroy_cb), NULL);

	panel_applet_setup_menu_from_file (PANEL_APPLET (applet), 
					   NULL,
					   "GNOME_Open_Applet.xml",
					   NULL,
					   open_applet_menu_verbs,
					   applet);

	return retval;
}

PANEL_APPLET_BONOBO_FACTORY ("OAFIID:GNOME_Open_Applet_Factory",
			     open_applet_get_type (),
			     "Open_Applet",
			     "0",
			     (PanelAppletFactoryCallback) open_applet_factory,
			     NULL)


